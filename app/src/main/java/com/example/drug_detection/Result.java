package com.example.drug_detection;
import org.opencv.core.Mat;

import java.util.List;

public class Result {
    private long time;
    private List<Mat> result;
    private int num_detections;
    private Mat main;

    public long getTime() {
        return time;
    }

    public List<Mat> getResult() {
        return result;
    }

    public int getNum_detections() {
        return num_detections;
    }

    public void setNum_detections(int num_detections) {
        this.num_detections = num_detections;
    }

    public void setResult(List<Mat> result) {
        this.result = result;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Mat getMain() {
        return main;
    }

    public void setMain(Mat main) {
        this.main = main;
    }
}
