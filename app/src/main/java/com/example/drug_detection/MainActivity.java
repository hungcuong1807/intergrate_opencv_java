package com.example.drug_detection;

import androidx.appcompat.app.AppCompatActivity;
import android.content.res.AssetManager;
import android.content.Context;
import android.os.Bundle;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.opencv.core.CvType;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Net;
import org.opencv.dnn.Dnn;
import org.opencv.android.Utils;
import android.os.SystemClock;
import org.opencv.imgproc.Imgproc;
import java.io.BufferedInputStream;

public class MainActivity extends AppCompatActivity {
    // Initialize OpenCV manager.

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect;
    private ImageView ivImage;
    private LinearLayout linearLayout1;
    private TextView num_detection;
    private TextView time;
    private String userChoosenTask;
    private Net net;
    private static final String TAG = "OpenCV/Sample/Mask_Rcnn";
    private static final String[] classNames = {"drug"};
    // Initialize OpenCV manager.
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    break;
                }
                default: {
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSelect = (Button) findViewById(R.id.btnSelectPhoto);
        btnSelect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        ivImage = (ImageView) findViewById(R.id.ivImage);
        num_detection = (TextView) findViewById(R.id.num_detection);
        time = (TextView) findViewById(R.id.time);
        linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo")) {

                        cameraIntent();
                    }

                    else if (userChoosenTask.equals("Choose from Library")) {

                        galleryIntent();
                    }

                } else {
                    // code for deny
                }
                break;
        }
    }

    private String getPath(String file, Context context) {
        AssetManager assetManager = getAssets();
        BufferedInputStream inputStream = null;
        try {
            // Read data from assets.
            inputStream = new BufferedInputStream(assetManager.open(file));
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
            // Create copy file in storage.
            File outFile = new File(context.getFilesDir(), file);
            FileOutputStream os = new FileOutputStream(outFile);
            os.write(data);
            os.close();
            // Return a path to file which may be read in common way.
            return outFile.getAbsolutePath();
        } catch (IOException ex) {
            Log.i(TAG, "Failed to upload a file");
        }
        return "";
    }

    public Result recognizeImage(Mat image) {
        Mat image_test = image.clone();
        Result rs = new Result();
        List<Mat> mats = new ArrayList<Mat>();
        String proto = getPath("graph.pbtxt", this);
        String weights = getPath("frozen_inference_graph.pb", this);

        net = Dnn.readNetFromTensorflow(weights, proto);
        final double THRESHOLD = 0.5;
        final double THRESHOLD_MASK = 0.3;
        int cols = image.cols();
        int rows = image.rows();
        Mat blob = Dnn.blobFromImage(image, 1, new Size(cols, rows), new Scalar(0, 0, 0), true,
                false);
        net.setInput(blob);
        List<Mat>outs = new ArrayList<Mat>();
        List<String> outputNames = new ArrayList<String>();
        outputNames.add("detection_out_final");
        outputNames.add("detection_masks");
        final long startTime = SystemClock.uptimeMillis();
        net.forward(outs, outputNames);
        long lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;
        Mat boxes = outs.get(0);
        Mat masks = outs.get(1);
        boxes = boxes.reshape(1, (int) boxes.total() / 7);
        int count = 0;
        for (int i = 0; i < boxes.rows(); i++) {
            Mat img = image_test.clone();
            double confidence = boxes.get(i, 2)[0];
            if (confidence > THRESHOLD) {
                count++;
                int classId = (int) boxes.get(i, 1)[0];
                int left = (int) (boxes.get(i, 3)[0] * cols);

                int top = (int) (boxes.get(i, 4)[0] * rows);
                int right = (int) (boxes.get(i, 5)[0] * cols);
                int bottom = (int) (boxes.get(i, 6)[0] * rows);

                int boxW = right - left;
                int boxH = bottom - top;
                Mat mask = masks.row(i);
                mask = mask.col(classId);

                mask = mask.reshape(1, 15);
                Mat newMask =new Mat();
                Size sz = new Size(boxW, boxH);
                Imgproc.resize(mask, newMask, sz, Imgproc.INTER_NEAREST);
                Random rand = new Random();
//
//                // color to mask rcnn
                double color_1 = 0.3 * rand.nextInt(255);
                double color_2 = 0.3 * rand.nextInt(255);
                double color_3 = 0.3 * rand.nextInt(255);
                for (int row = 0; row < newMask.rows(); row++) {
                    for (int col = 0; col < newMask.cols(); col++) {
                        double data = (double) newMask.get(row, col)[0];
                        if (data > THRESHOLD_MASK) {
                            double[] data_image = img.get(top+row, left+col);
                            data_image[0] = color_1 + 0.3 * data_image[0];
                            data_image[1] = color_2 + 0.3 * data_image[1];

                            data_image[2] = color_3 + 0.3 * data_image[2];
                            img.put(top+row, left+col, data_image);
                        }
                    }
                }
                // Draw rectangle around detected object.
                Imgproc.rectangle(img, new Point(left, top), new Point(right, bottom), new Scalar(color_1, color_2,color_3), 3);
                String label = classNames[classId] + ": " + confidence;
                int[] baseLine = new int[1];
                Size labelSize = Imgproc.getTextSize(label, Imgproc.FONT_HERSHEY_SIMPLEX, 0.5, 1, baseLine);
                // Draw background for label.
                Imgproc.rectangle(img, new Point(left, top - labelSize.height),
                        new Point(left + labelSize.width, top + baseLine[0]), new Scalar(255, 255,255), 2);
                // Write class name and confidence.
                Imgproc.putText(img, label, new Point(left, top), Imgproc.FONT_HERSHEY_SIMPLEX, 0.5,
                        new Scalar(0, 0, 0));
                mats.add(img);
            }
        }
        rs.setResult(mats);
        rs.setMain(image_test);
        rs.setTime(lastProcessingTimeMs);
        rs.setNum_detections(count);
        return rs;
    }

    private void selectImage() {
        final CharSequence[] items = {"Choose from Library", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(MainActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    if (result) {
                        cameraIntent();
                    }

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        linearLayout1.removeAllViews();

        Bitmap bm = null;
        List<Mat>mats = new ArrayList<Mat>();
        Result rs = new Result();
        String t = "";
        String num_detections = "";
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Mat mat = new Mat(bm.getWidth(), bm.getHeight(), CvType.CV_8UC4);
                Utils.bitmapToMat(bm, mat);
                Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGBA2BGR);
                rs = recognizeImage(mat);
                mats = rs.getResult();
                t+=rs.getTime();
                num_detections+=rs.getNum_detections();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
        num_detection.setText(num_detections);
        time.setText(t);
        for(int i = 0; i< mats.size(); i++){
            ImageView image = new ImageView(MainActivity.this);
            Bitmap bmp = Bitmap.createBitmap(mats.get(i).width(), mats.get(i).height(), Bitmap.Config.ARGB_8888);
            Mat new_mat = new Mat();
            Imgproc.cvtColor(mats.get(i), new_mat, Imgproc.COLOR_BGR2RGBA);
            Utils.matToBitmap(new_mat, bmp);
            image.setImageBitmap(bmp);
            linearLayout1.addView(image);
        }
    }

}
